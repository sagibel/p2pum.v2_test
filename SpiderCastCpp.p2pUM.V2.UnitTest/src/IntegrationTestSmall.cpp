/*
 * IntegrationTest1.cpp
 *
 *  Created on: Jun 2, 2013
 *      Author: user
 */

#include <boost/test/unit_test.hpp>

#include "TestUtils.hpp"

#include "QueueManager.h"
#include "ServiceThread.h"
#include "Listener.h"
#include "ConnectionHandlerImpl.h"
#include "Communicator.h"
#include "ConnectionImpl.h"
#include "OutgoingStreamImpl.h"
#include "TimerHandler.h"
#include "Network_Messaging/StreamRequestMessage.h"
#include "Network_Messaging/CloseStreamMessage.h"
#include "Network_Messaging/TxMessageImpl.h"


//=== Core_suite ===
BOOST_AUTO_TEST_SUITE( Core_suite )

/**
 * Just say hello
 */
BOOST_AUTO_TEST_CASE( integration_small )
{
	using namespace p2pum;

	std::cout << "IntegrationTestSmall started" << std::endl;

	ConnectionManager_SPtr cm(new ConnectionManager(PropertyMap()));
	QueueManager_SPtr qm(new QueueManager(PropertyMap()));
	IOService_SPtr service(new boost::asio::io_service);
	ServiceThread_SPtr serviceThread(new ServiceThread(service));
	serviceThread->start();
	MessageListener* ml = new MessageListenerImpl();
	EventListener* el = new EventListenerImpl();
	TimerHandler_SPtr th(new TimerHandler(qm, cm));

	Listener_SPtr acceptor(new Listener(service, 5000, th));
	Handler_SPtr handler(new ConnectionHandlerImpl(cm, qm, acceptor));
	Communicator_SPtr com(new Communicator(el, new LogListenerImpl(), qm, cm, handler));
	com->start();
	acceptor->listen(handler);

	Connectable_SPtr con(new ConnectionImpl(service, handler, th));
	Connection_SPtr _con = boost::dynamic_pointer_cast<Connection, Connectable>(con);
	cm->addConnection(boost::dynamic_pointer_cast<Connection, Connectable>(con));
	cm->updateConnectionStatus(_con->getConnectionID(), Connection::OutgoingPending);
	tcp::resolver resolver(*service);
	tcp::resolver::query query("127.0.0.1", "5000");
	tcp::resolver::iterator iterator = resolver.resolve(query);
	con->connect(*iterator);

	sleep(1);

	Stream_SPtr stream(new OutgoingStreamImpl(_con->getConnectionID(), PropertyMap(), th, service));
	cm->addStream(stream);
	cm->updateStreamStatus(_con->getConnectionID(), stream->getLocalStreamID(), AbstractStream::Pending);
	Message_SPtr message1(new StreamRequestMessage(stream->getLocalStreamID(), stream->getProps()));
	con->writeToSocket(message1); //create new stream - should fail because not accepting streams & no ml.

	sleep(1);

	ConnectionHandlerImpl* handlerImpl = dynamic_cast<ConnectionHandlerImpl*>(handler.get());
	handlerImpl->acceptStreams(true);
	com->registerMessageListener(ml);
	con->writeToSocket(message1); //create new stream - should work fine.

	sleep(1);

	char* content = "hello";
	Message_SPtr message2(new TxMessageImpl(stream->getPeerStreamID(), content, sizeof(content))); //make sure peer stream id was updated
	con->writeToSocket(message2);

	sleep(1);

	StreamID id = stream->getPeerStreamID();
	Message_SPtr message3(new CloseStreamMessage(id));
	con->writeToSocket(message3);
	cm->updateStreamStatus(_con->getConnectionID(), stream->getLocalStreamID(), AbstractStream::Closed);

	sleep(1);

	_con->close();

	sleep(5); //let operations finish

	acceptor->close();
	com->stop();
	com->join();
	serviceThread->stop();
	serviceThread->join();


	std::cout << "IntegrationTestSmall finished successfully" << std::endl;

}

BOOST_AUTO_TEST_SUITE_END() // Core_suite


