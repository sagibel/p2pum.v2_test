/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

/*
 * Test1.cpp
 *
 *  Created on: Nov 7, 2012
 *      Author: tock
 */

#include <stdio.h>

#include <boost/test/unit_test.hpp>

#include "P2PUM.h"

namespace p2pum {
	namespace test {

	class EventListenerStub : public p2pum::EventListener
	{
	public:
		EventListenerStub(){}

		virtual ~EventListenerStub(){}

		p2pum::EventReturnCode onEvent(const p2pum::Event& event )
		{
			return p2pum::Event_RC_NoOp;
		}
	};


	class LogListenerStub : public p2pum::LogListener
	{

		void onLogMessage(LogLevel logLevel, uint64_t timeStamp,
				uint64_t threadId, std::string message)
		{
			std::cout << timeStamp << ": " << threadId << ": " << message << std::endl;
		}

	};


	}  // namespace test
}  // namespace p2pup

//=== Core_suite ===
BOOST_AUTO_TEST_SUITE( Core_suite )

/**
 * Just say hello
 */
BOOST_AUTO_TEST_CASE( create_instance )
{
	BOOST_TEST_MESSAGE("==>>");
	using namespace p2pum;

	PropertyMap prop;
	prop["port"] = "5000";
	p2pum::test::EventListenerStub el;
	p2pum::test::LogListenerStub ll;

	p2pum::P2PUM_SPtr instance = p2pum::P2PUM::createInstance(prop, &el, &ll);

	std::cout << "Hello Instance" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END() // Core_suite

//EOF


