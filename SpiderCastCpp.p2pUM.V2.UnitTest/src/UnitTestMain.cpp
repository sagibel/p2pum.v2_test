/**********************************************************************
 * Licensed Materials - Property of IBM
 *
 * (C) Copyright IBM Corp. 2012.  All Rights Reserved.
 *
 * US Government Users Restricted Rights - Use, duplication or
 * disclosure restricted by GSA ADP Schedule Contract with
 * IBM Corp.
 **********************************************************************
 */

#define BOOST_TEST_MODULE p2pUM_UnitTestMain

#include <boost/test/unit_test.hpp>
#include <string>
#include <iostream>

#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/bind.hpp>
#include <boost/thread/condition.hpp>
#include <boost/thread/tss.hpp>
#include <boost/thread/once.hpp>
#include <boost/shared_ptr.hpp>

//=== Core_suite ===
BOOST_AUTO_TEST_SUITE( Core_suite )

/**
 * Just say hello
 */
BOOST_AUTO_TEST_CASE( HelloWorld )
{
	BOOST_TEST_MESSAGE("==>>");


	std::cout << "This is thread (main): " << boost::this_thread::get_id() << std::endl;
	std::string cs0( "Hello World!" );                                                 // 1 //
	BOOST_CHECK_EQUAL( cs0.length(), (size_t)12 );

	std::cout << cs0 << std::endl;


}

BOOST_AUTO_TEST_SUITE_END() // Core_suite

//EOF


