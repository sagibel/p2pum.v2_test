/*
 * IntegrationTestBig.cpp
 *
 *  Created on: Jun 16, 2013
 *      Author: user
 */

#include <boost/test/unit_test.hpp>

#include "TestUtils.hpp"

#include "StreamRx.h"
#include "StreamTx.h"
#include "P2PUM.h"
#include "P2PUM_Definitions.h"
#include "Network_Messaging/TxMessageImpl.h"

//=== Core_suite ===
BOOST_AUTO_TEST_SUITE( Core_suite )

/**
 * Just say hello
 */
BOOST_AUTO_TEST_CASE( integration_big )
{
	using namespace p2pum;

	std::cout << "IntegrationTestBig started" << std::endl;

	try{
		EventListenerOther* el = new EventListenerOther();
		PropertyMap map;
		map["port"] = "5000";
		P2PUM_SPtr instance1 = P2PUM::createInstance(map, el, new LogListenerImpl());
		map["port"] = "5001";
		P2PUM_SPtr instance2 = P2PUM::createInstance(map, el, new LogListenerImpl());
		ConnectionParams params;
		params.heartbeat_interval_milli = 1000;//1sec
		params.heartbeat_timeout_milli = 5000;//5sec
		params.address = "127.0.0.1";
		params.connect_msg = "";
		params.context = 0;
		params.establish_timeout_milli = 3000; //3sec
		params.port = 5000;
		instance2->establishConnection(params);
		sleep(1); //let connection establish
		PropertyMap props;
		props["heartbeat_interval"] = "5000";//5sec
		props["heartbeat_timeout"] = "10000";//10sec
		StreamRx_SPtr streamRx1 = instance1->createStreamRx(props, new MessageListenerImpl());
		StreamTx_SPtr streamTx1 = instance2->createStreamTx(props, el->outgoingC);
		sleep(1); //let stream establish
		char* content = "hello";
		Message_SPtr message(new TxMessageImpl(streamTx1->getStreamID(), content, sizeof(content)));
		TxMessage* messageTx = dynamic_cast<TxMessageImpl*>(message.get());
		streamTx1->sendMessage(*messageTx);
		sleep(1); //let message play
		streamRx1->rejectStream(el->incomingC->getConnectionID(), el->incomingS);
		sleep(1);
		streamRx1->close();
		sleep(1); //let stream receiver close
		try{
			streamTx1->sendMessage(*messageTx); //should fail!
		}
		catch(P2PUMException e){
			std::cout << std::string(e.what()) << std::endl;
		}
		StreamTx_SPtr streamTx2 = instance2->createStreamTx(props, el->outgoingC); //should fail!
		sleep(1);
		StreamRx_SPtr streamRx2 = instance1->createStreamRx(props, new MessageListenerImpl());
		StreamTx_SPtr streamTx3 = instance2->createStreamTx(props, el->outgoingC);
		sleep(1);
		streamTx3->sendMessage(*messageTx); //should be ok again
		sleep(1);
		streamTx3->close(true);
		sleep(1);//let stream close gracefully
		StreamTx_SPtr streamTx4 = instance2->createStreamTx(props, el->outgoingC);
		StreamTx_SPtr streamTx5 = instance2->createStreamTx(props, el->outgoingC);
		sleep(1); //let streams establish
		streamRx2->close();
		sleep(1); //let streams close
		try{
			streamTx4->sendMessage(*messageTx); //should fail!
		}
		catch(P2PUMException e){
			std::cout << std::string(e.what()) << std::endl;
		}
		sleep(1);
		el->outgoingC->close();
		sleep(1); //let connection close gracefully
		instance1->close();
		instance2->close();
	}
	catch(P2PUMException e){
		std::cout << std::string(e.what()) << std::endl;
	}
	catch(std::exception e){
		std::cout << std::string(e.what()) << std::endl;
	}

	std::cout << "IntegrationTestBig finished successfully" << std::endl;

}

BOOST_AUTO_TEST_SUITE_END() // Core_suite




