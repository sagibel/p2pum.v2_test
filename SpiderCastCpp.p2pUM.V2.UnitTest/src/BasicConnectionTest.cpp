/*
 * BasicConnectionTest.cpp
 *
 *  Created on: May 4, 2013
 *      Author: p2pm
 */

#include <boost/test/unit_test.hpp>

#include "TestUtils.hpp"

#include "ConnectionImpl.h"
#include "Expirable.h"
#include "Refreshable.h"
#include "ServiceThread.h"
#include "TimerHandler.h"
#include "Network_Messaging/CloseConnectionMessage.h"
#include "Network_Messaging/TxMessageImpl.h"

//=== Core_suite ===
BOOST_AUTO_TEST_SUITE( Core_suite )

/**
 * Just say hello
 */
BOOST_AUTO_TEST_CASE( connection )
{
	using namespace p2pum;

	std::cout << "BasicConnectionTest started" << std::endl;

	ConnectionManager_SPtr cm(new ConnectionManager(PropertyMap()));
	IOService_SPtr service(new boost::asio::io_service);
	ServiceThread_SPtr serviceThread(new ServiceThread(service));
	serviceThread->start();
	TimerHandler_SPtr th(new TimerHandler(QueueManager_SPtr(new QueueManager(PropertyMap())), cm));
	Listener_SPtr acceptor(new Listener(service, 5000, th));
	Handler_SPtr handler(new EmptyHandler(cm));
	acceptor->listen(handler);

	Connectable_SPtr con(new ConnectionImpl(service, handler, th));
	cm->addConnection(boost::dynamic_pointer_cast<Connection, Connectable>(con));
	tcp::resolver resolver(*service);
	tcp::resolver::query query("127.0.0.1", "5000");
	tcp::resolver::iterator iterator = resolver.resolve(query);
	con->connect(*iterator);

	sleep(1);

	char* content = "hello";
	Message_SPtr message(new TxMessageImpl(NO_ID, content, sizeof(content)));
	con->writeToSocket(message);

	sleep(1);

	Connection_SPtr _con = boost::dynamic_pointer_cast<Connection, Connectable>(con);
	_con->close();

	sleep(1);

	acceptor->close();
	serviceThread->stop();
	serviceThread->join();

	std::cout << "BasicConnectionTest finished successfully" << std::endl;

}

BOOST_AUTO_TEST_SUITE_END() // Core_suite


