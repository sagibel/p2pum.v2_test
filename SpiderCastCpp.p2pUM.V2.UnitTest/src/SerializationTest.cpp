/*
 * SerializationTest.cpp
 *
 *  Created on: Apr 7, 2013
 *      Author: leo9385
 */


#include <boost/test/unit_test.hpp>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/tmpdir.hpp>

#include <fstream>

#include "Network_Messaging/TxMessageImpl.h"
#include "Network_Messaging/StreamResponseMessage.h"
#include "Network_Messaging/ConnectionHeartbeatMessage.h"
#include "Network_Messaging/ConnectionRequestMessage.h"
#include "Network_Messaging/ConnectionResponseMessage.h"
#include "Network_Messaging/StreamRequestMessage.h"
#include "Network_Messaging/StreamHeartbeatMessage.h"
#include "Network_Messaging/CloseConnectionMessage.h"
#include "Network_Messaging/CloseStreamMessage.h"


//=== Core_suite ===
BOOST_AUTO_TEST_SUITE( Core_suite )

/**
 * Just say hello
 */
BOOST_AUTO_TEST_CASE( serialization )
{
	using namespace p2pum;

	std::cout << "SerializationTest started" << std::endl;

    std::string filename(boost::archive::tmpdir());
    filename += "/testfile";

	std::ofstream ofs(filename.c_str());
	boost::archive::text_oarchive archive1(ofs);

	char* content = "bla";
	ConnectionParams params;
	params.address = "127.0.0.1";
	params.establish_timeout_milli = 10000;
	params.heartbeat_interval_milli = 1000;
	params.heartbeat_timeout_milli = 10000;
	params.port = 5001;
	PropertyMap props;
	props["timeout"] = "10000";
	props["interval"] = "1000";
	Message_SPtr message1in(new TxMessageImpl(1, content, sizeof(content)));
	Message_SPtr message2in(new StreamResponseMessage(1, true));
	Message_SPtr message3in(new ConnectionHeartbeatMessage(1));
	Message_SPtr message4in(new ConnectionRequestMessage(1, params));
	Message_SPtr message5in(new ConnectionResponseMessage(1, true));
	Message_SPtr message6in(new StreamRequestMessage(1, props));
	Message_SPtr message7in(new StreamHeartbeatMessage(1));
	Message_SPtr message8in(new CloseConnectionMessage(1));
	Message_SPtr message9in(new CloseStreamMessage(1));

	archive1 << message1in;
	archive1 << message2in;
	archive1 << message3in;
	archive1 << message4in;
	archive1 << message5in;
	archive1 << message6in;
	archive1 << message7in;
	archive1 << message8in;
	archive1 << message9in;

	Message_SPtr message10in(new CloseStreamMessage(2));
	Message_SPtr message11in(new CloseStreamMessage(3));
	archive1 << message10in;
	archive1 << message11in;

	ofs.flush();
	ofs.close();

	Message_SPtr message1out;
	Message_SPtr message2out;
	Message_SPtr message3out;
	Message_SPtr message4out;
	Message_SPtr message5out;
	Message_SPtr message6out;
	Message_SPtr message7out;
	Message_SPtr message8out;
	Message_SPtr message9out;

    std::ifstream ifs(filename.c_str());
	boost::archive::text_iarchive archive2(ifs);
	archive2 >> message1out;
	archive2 >> message2out;
	archive2 >> message3out;
	archive2 >> message4out;
	archive2 >> message5out;
	archive2 >> message6out;
	archive2 >> message7out;
	archive2 >> message8out;
	archive2 >> message9out;

    BOOST_CHECK_EQUAL(message1in->getMessageID(), message1out->getMessageID());
    BOOST_CHECK_EQUAL(message1in->getType(), message1out->getType());
    BOOST_CHECK_EQUAL(message2in->getMessageID(), message2out->getMessageID());
    BOOST_CHECK_EQUAL(message2in->getType(), message2out->getType());
    BOOST_CHECK_EQUAL(message3in->getMessageID(), message3out->getMessageID());
    BOOST_CHECK_EQUAL(message3in->getType(), message3out->getType());
    BOOST_CHECK_EQUAL(message4in->getMessageID(), message4out->getMessageID());
    BOOST_CHECK_EQUAL(message4in->getType(), message4out->getType());
    BOOST_CHECK_EQUAL(message5in->getMessageID(), message5out->getMessageID());
    BOOST_CHECK_EQUAL(message5in->getType(), message5out->getType());
    BOOST_CHECK_EQUAL(message6in->getMessageID(), message6out->getMessageID());
    BOOST_CHECK_EQUAL(message6in->getType(), message6out->getType());
    BOOST_CHECK_EQUAL(message7in->getMessageID(), message7out->getMessageID());
    BOOST_CHECK_EQUAL(message7in->getType(), message7out->getType());
    BOOST_CHECK_EQUAL(message8in->getMessageID(), message8out->getMessageID());
    BOOST_CHECK_EQUAL(message8in->getType(), message8out->getType());
    BOOST_CHECK_EQUAL(message9in->getMessageID(), message9out->getMessageID());
    BOOST_CHECK_EQUAL(message9in->getType(), message9out->getType());

	Message_SPtr message10out;
	Message_SPtr message11out;
	archive2 >> message10out;
	archive2 >> message11out;

    BOOST_CHECK_EQUAL(dynamic_cast<CloseStreamMessage*>(message10in.get())->streamID, dynamic_cast<CloseStreamMessage*>(message10out.get())->streamID);
    BOOST_CHECK_EQUAL(dynamic_cast<CloseStreamMessage*>(message11in.get())->streamID, dynamic_cast<CloseStreamMessage*>(message11out.get())->streamID);

    ifs.close();
    remove(filename.c_str());

	std::cout << "SerializationTest finished successfully" << std::endl;
}

BOOST_AUTO_TEST_SUITE_END() // Core_suite

//EOF

