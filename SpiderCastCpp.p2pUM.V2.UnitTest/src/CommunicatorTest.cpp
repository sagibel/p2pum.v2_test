/*
 * CommunicatorTest.cpp
 *
 *  Created on: Apr 7, 2013
 *      Author: leo9385
 */

#include <boost/test/unit_test.hpp>

#include "TestUtils.hpp"

#include "ConnectionImpl.h"
#include "Communicator.h"
#include "ConnectionHandlerImpl.h"
#include "Listener.h"
#include "QueueManager.h"
#include "TimerHandler.h"

using namespace p2pum;

void testMethod(IOService_SPtr service, Communicator_SPtr comm, Handler_SPtr handler, QueueManager_SPtr qm, TimerHandler_SPtr th){
	Connection_SPtr con1(new ConnectionImpl(service, handler, th));
	Event_SPtr event1 = Event_SPtr(new EventImpl(Event::Connection_Establish_Success, con1, con1->getConnectionID()));
	qm->pushEvent(event1);
	Connection_SPtr con2(new ConnectionImpl(service, handler, th));
	Event_SPtr event2 = Event_SPtr(new EventImpl(Event::Connection_Establish_Success, con2, con2->getConnectionID()));
	qm->pushEvent(event2);
	Connection_SPtr con3(new ConnectionImpl(service, handler, th));
	Event_SPtr event3 = Event_SPtr(new EventImpl(Event::Connection_Establish_Success, con3, con3->getConnectionID()));
	qm->pushEvent(event3);

	qm->pushEvent(event1);
	qm->pushEvent(event2); //check dups ok
}


//=== Core_suite ===
BOOST_AUTO_TEST_SUITE( Core_suite )

/**
 * Just say hello
 */
BOOST_AUTO_TEST_CASE( communicator )
{
	using namespace p2pum;

	std::cout << "CommunicatorTest started" << std::endl;

	IOService_SPtr service(new boost::asio::io_service);

	QueueManager_SPtr qm(new QueueManager(PropertyMap()));
	ConnectionManager_SPtr cm(new ConnectionManager(PropertyMap()));
	TimerHandler_SPtr th(new TimerHandler(qm, cm));
	Listener_SPtr listener(new Listener(service, 5000, th));
	Handler_SPtr handler(new ConnectionHandlerImpl(cm, qm, listener));
	Communicator_SPtr comm(new Communicator(new EventListenerImpl(), new LogListenerImpl(), qm, cm, handler));
	comm->start();
	boost::thread testThread(testMethod, service, comm, handler, qm, th);

	testThread.join();
	sleep(5); //roughly appropriate time for communicator to complete all event pops
	comm->registerMessageListener(new MessageListenerImpl());
	RxMessage_SPtr mes1(new RxMessageImpl("1st message", 0, 0, 0));
	RxMessage_SPtr mes2(new RxMessageImpl("2nd message", 0, 0, 0));
	qm->pushMessage(mes1);
	qm->pushMessage(mes2);
	sleep(1); //let communicator deal with messages
	comm->unregisterMessageListener();
	qm->pushMessage(mes1);
	qm->pushMessage(mes2);
	sleep(1); //let rest of pops finish...last messages shouldn't be served
	listener->close();
	comm->stop();
	comm->join();

	std::cout << "CommunicatorTest finished successfully" << std::endl;

}

BOOST_AUTO_TEST_SUITE_END() // Core_suite

//EOF
